package ventanaeclipse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import javax.swing.JMenuBar;
import java.awt.Choice;
import java.awt.Scrollbar;
import java.awt.Button;
import java.awt.List;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.JTextField;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JProgressBar;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.JToolBar;
import javax.swing.JMenu;
import javax.swing.JTabbedPane;
import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JRadioButtonMenuItem;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPasswordField passwordField_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Viajamos Contigo");
		setIconImage(Toolkit.getDefaultToolkit().getImage("I:\\1\u00BAGS\\ENTORNOS DE DESARROLLO\\bus1.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 684, 513);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("Opciones");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("Acceder como Usuario Invitado");
		menu.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("Mantener ubicacion activada");
		menu.add(menuItem_1);
		
		JMenu mnOpciones = new JMenu("Recarga de Tarjetas Online");
		menuBar.add(mnOpciones);
		
		JMenuItem mntmAccederComoUsuario = new JMenuItem("Iniciar Sesion en gmail");
		mnOpciones.add(mntmAccederComoUsuario);
		
		JMenuItem mntmMantenerUbicacionActivada = new JMenuItem("Crear Cuenta");
		mnOpciones.add(mntmMantenerUbicacionActivada);
		
		JMenuItem mntmSoloRecargas = new JMenuItem("Solo recargas");
		mnOpciones.add(mntmSoloRecargas);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar_1 = new JToolBar();
		toolBar_1.setBackground(new Color(255, 240, 245));
		toolBar_1.setFont(new Font("Segoe UI", Font.BOLD, 12));
		toolBar_1.setForeground(Color.BLUE);
		toolBar_1.setBounds(33, 42, 478, 23);
		contentPane.add(toolBar_1);
		
		JButton btnParadasCercanas = new JButton("Paradas cercanas");
		btnParadasCercanas.setBackground(new Color(255, 240, 245));
		toolBar_1.add(btnParadasCercanas);
		
		JButton btnComprarTicketDe = new JButton("Comprar ticket de transporte");
		btnComprarTicketDe.setBackground(new Color(255, 240, 245));
		toolBar_1.add(btnComprarTicketDe);
		
		JButton btnInformacionDeLas = new JButton("Informacion de las lineas");
		btnInformacionDeLas.setBackground(new Color(255, 240, 245));
		toolBar_1.add(btnInformacionDeLas);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 88, 643, 355);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		tabbedPane.addTab("Iniciar Sesion", null, panel, null);
		panel.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(269, 39, 100, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(269, 70, 100, 20);
		panel.add(passwordField);
		
		JLabel lblUsuario = new JLabel("USUARIO");
		lblUsuario.setForeground(Color.BLUE);
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUsuario.setBounds(168, 35, 78, 26);
		panel.add(lblUsuario);
		
		JLabel lblContrasea_1 = new JLabel("CONTRASE\u00D1A");
		lblContrasea_1.setForeground(Color.BLUE);
		lblContrasea_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblContrasea_1.setBounds(168, 66, 91, 26);
		panel.add(lblContrasea_1);
		
		JToggleButton tglbtnEntrar = new JToggleButton("ENTRAR");
		tglbtnEntrar.setForeground(Color.WHITE);
		tglbtnEntrar.setFont(new Font("Tahoma", Font.BOLD, 11));
		tglbtnEntrar.setBackground(Color.BLUE);
		tglbtnEntrar.setBounds(229, 124, 106, 23);
		panel.add(tglbtnEntrar);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 240));
		tabbedPane.addTab("Registro", null, panel_1, null);
		panel_1.setLayout(null);
		
		JToggleButton tglbtnCrearCuenta = new JToggleButton("CREAR CUENTA");
		tglbtnCrearCuenta.setFont(new Font("Tahoma", Font.BOLD, 11));
		tglbtnCrearCuenta.setBackground(new Color(255, 215, 0));
		tglbtnCrearCuenta.setForeground(Color.WHITE);
		tglbtnCrearCuenta.setBounds(262, 282, 144, 23);
		panel_1.add(tglbtnCrearCuenta);
		
		textField_2 = new JTextField();
		textField_2.setBounds(191, 29, 100, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(13, 13, 100, 1));
		spinner.setBounds(397, 29, 46, 20);
		panel_1.add(spinner);
		
		textField_3 = new JTextField();
		textField_3.setBounds(191, 60, 100, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(191, 95, 100, 20);
		panel_1.add(passwordField_1);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setBackground(new Color(255, 255, 240));
		buttonGroup_1.add(rdbtnHombre);
		rdbtnHombre.setBounds(398, 81, 87, 23);
		panel_1.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBackground(new Color(255, 255, 240));
		buttonGroup_1.add(rdbtnMujer);
		rdbtnMujer.setBounds(481, 81, 68, 23);
		panel_1.add(rdbtnMujer);
		
		JRadioButton rdbtnOtro = new JRadioButton("Otro");
		rdbtnOtro.setBackground(new Color(255, 255, 240));
		buttonGroup_1.add(rdbtnOtro);
		rdbtnOtro.setBounds(551, 81, 69, 23);
		panel_1.add(rdbtnOtro);
		
		JLabel lblNombreUsuario = new JLabel("NOMBRE USUARIO");
		lblNombreUsuario.setForeground(Color.BLUE);
		lblNombreUsuario.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNombreUsuario.setBounds(28, 25, 165, 26);
		panel_1.add(lblNombreUsuario);
		
		JLabel lblCorreoElectronico = new JLabel("CORREO ELECTRONICO");
		lblCorreoElectronico.setForeground(Color.BLUE);
		lblCorreoElectronico.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCorreoElectronico.setBounds(26, 60, 153, 26);
		panel_1.add(lblCorreoElectronico);
		
		JLabel lblContrasea = new JLabel("CONTRASE\u00D1A");
		lblContrasea.setForeground(Color.BLUE);
		lblContrasea.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblContrasea.setBounds(28, 91, 153, 26);
		panel_1.add(lblContrasea);
		
		JLabel lblSexo = new JLabel("SEXO");
		lblSexo.setForeground(Color.BLUE);
		lblSexo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSexo.setBounds(345, 78, 61, 26);
		panel_1.add(lblSexo);
		
		JLabel lblEdad = new JLabel("EDAD ");
		lblEdad.setForeground(Color.BLUE);
		lblEdad.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEdad.setBounds(345, 25, 46, 26);
		panel_1.add(lblEdad);
		
		JLabel lblserMayorA = new JLabel("(ser mayor a 13 a\u00F1os)");
		lblserMayorA.setForeground(Color.BLUE);
		lblserMayorA.setBounds(455, 28, 129, 23);
		panel_1.add(lblserMayorA);
		
		JLabel lblFoto = new JLabel("FOTO");
		lblFoto.setForeground(Color.BLUE);
		lblFoto.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblFoto.setBounds(26, 150, 153, 26);
		panel_1.add(lblFoto);
		
		JCheckBox chckbxEstablecerMasAdelante = new JCheckBox("Establecer mas adelante la foto");
		chckbxEstablecerMasAdelante.setBackground(new Color(255, 255, 240));
		chckbxEstablecerMasAdelante.setForeground(Color.BLUE);
		chckbxEstablecerMasAdelante.setBounds(30, 252, 247, 23);
		panel_1.add(chckbxEstablecerMasAdelante);
		
		JButton btnImagen = new JButton("Insertar Imagen");
		btnImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnImagen.setBackground(new Color(255, 255, 255));
		btnImagen.setForeground(Color.BLUE);
		btnImagen.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/image1.png")));
		btnImagen.setBounds(89, 159, 226, 86);
		panel_1.add(btnImagen);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(240, 255, 240));
		tabbedPane.addTab("Sobre la aplicacion", null, panel_2, null);
		panel_2.setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(208, 62, 261, 26);
		panel_2.add(progressBar);
		progressBar.setForeground(new Color(60, 179, 113));
		progressBar.setValue(67);
		
		JSlider slider = new JSlider();
		slider.setBackground(new Color(240, 255, 240));
		slider.setMinimum(1);
		slider.setMaximum(10);
		slider.setMajorTickSpacing(1);
		slider.setMinorTickSpacing(1);
		slider.setBounds(220, 177, 200, 55);
		panel_2.add(slider);
		slider.setValue(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		
		JLabel lblEvaluaLaAplicacion = new JLabel("EVALUA LA APLICACION");
		lblEvaluaLaAplicacion.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEvaluaLaAplicacion.setForeground(Color.BLUE);
		lblEvaluaLaAplicacion.setBounds(245, 152, 175, 14);
		panel_2.add(lblEvaluaLaAplicacion);
		
		JLabel lblUsoDeLa = new JLabel("USO DE LA APLICACION POR LOS USUARIOS");
		lblUsoDeLa.setForeground(Color.BLUE);
		lblUsoDeLa.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUsoDeLa.setBounds(198, 31, 275, 26);
		panel_2.add(lblUsoDeLa);
		
		JToggleButton tglbtnEnviarEvaluacion = new JToggleButton("ENVIAR EVALUACION");
		tglbtnEnviarEvaluacion.setForeground(Color.WHITE);
		tglbtnEnviarEvaluacion.setFont(new Font("Tahoma", Font.BOLD, 11));
		tglbtnEnviarEvaluacion.setBackground(new Color(60, 179, 113));
		tglbtnEnviarEvaluacion.setBounds(230, 245, 175, 23);
		panel_2.add(tglbtnEnviarEvaluacion);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(255, 240, 245));
		tabbedPane.addTab("Opiniones", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lbldinosTuOpinion = new JLabel("\u00A1DINOS TU OPINION!");
		lbldinosTuOpinion.setForeground(Color.BLUE);
		lbldinosTuOpinion.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbldinosTuOpinion.setBounds(241, 34, 198, 26);
		panel_3.add(lbldinosTuOpinion);
		
		JToggleButton tglbtnEnviar = new JToggleButton("ENVIAR");
		tglbtnEnviar.setForeground(Color.WHITE);
		tglbtnEnviar.setFont(new Font("Tahoma", Font.BOLD, 11));
		tglbtnEnviar.setBackground(Color.BLUE);
		tglbtnEnviar.setBounds(163, 251, 105, 23);
		panel_3.add(tglbtnEnviar);
		
		JToggleButton tglbtnCancelar = new JToggleButton("CANCELAR");
		tglbtnCancelar.setForeground(Color.WHITE);
		tglbtnCancelar.setFont(new Font("Tahoma", Font.BOLD, 11));
		tglbtnCancelar.setBackground(new Color(220, 20, 60));
		tglbtnCancelar.setBounds(359, 251, 105, 23);
		panel_3.add(tglbtnCancelar);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(153, 71, 319, 169);
		panel_3.add(textArea);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(new Color(255, 240, 245));
		toolBar.setFont(new Font("Segoe UI", Font.BOLD, 12));
		toolBar.setForeground(Color.BLUE);
		toolBar.setBounds(33, 11, 478, 23);
		contentPane.add(toolBar);
		
		JButton btnRecordarContrasea = new JButton("Recordar Contrase\u00F1a");
		btnRecordarContrasea.setBackground(new Color(255, 240, 245));
		toolBar.add(btnRecordarContrasea);
		
		JButton btnMantenerSesionIniciada = new JButton("Mantener Sesion Iniciada");
		btnMantenerSesionIniciada.setBackground(new Color(255, 240, 245));
		toolBar.add(btnMantenerSesionIniciada);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
