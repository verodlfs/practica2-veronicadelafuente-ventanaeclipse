package main;

import java.util.Scanner;

import proyecto.Libreria;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner (System.in);
		
	int cantidad;
	int opcion;
	String elegir;
	
	System.out.println("¿Cuantos numeros quieres introducir?");
	cantidad = in.nextInt();
	
	int[] numero = new int [cantidad];
	
	Libreria.rellenarNumero(numero);
	
	do {
		System.out.println();
	System.out.println("\t*** MENU: ***");
	System.out.println("1. Visualizar los numeros con su posicion.");
	System.out.println("2. Media de todos los numeros introducidos por teclado.");
	System.out.println("3. Decir el numero maximo y minimo.");
	System.out.println("4. Buscar un valor y cuantas veces se repite.");
	System.out.println("5. Salir.");
	System.out.println("Introducir una opcion = ");
	opcion = in.nextInt();
	
	
	if(opcion==1) {
		Libreria.visualizarNumero(numero);
	}
	if(opcion==2) {
		float resultado = Libreria.media(numero);
		System.out.println("La media de todos los numeros introducidos es = " + resultado);
	}
	if(opcion==3) {
		int max = Libreria.numeroMax(numero);
		System.out.println("El numero " + max + " es el valor maximo.");
		int min = Libreria.numeroMin(numero);
		System.out.println("El numero " + min + " es el valor minimo.");
	}
	if(opcion==4) {
		int cantidadRepeticion=0;
		System.out.println("¿Cuantos numeros quieres buscar?");
		cantidadRepeticion= in.nextInt();
		int[] numeroRepetido = new int [cantidadRepeticion];
		Libreria.vecesRepetido(numero, numeroRepetido);
	}
	}while(opcion==1 || opcion==2 ||opcion==3 ||opcion==4);
	
	
	
	}

}
